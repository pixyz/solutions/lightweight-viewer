# What is the Lightweight Viewer?

The Lightweight Viewer project is a Unity project using the Pixyz Plugin for Unity. Import your data in the Editor and press Play.

# Required Software

![](Documents/unity.png)|![](Documents/logo-plugin-mini.png)|
:-:|:-:|
Unity | Plugin for Unity