﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipPlane : MonoBehaviour
{
    public enum AxisMode
    {
        Axis_X,
        Axis_Y,
        Axis_Z,
        FreeMode
    }

    private List<Material> _materials;
    public List<Material> Materials {
        get
        {
            return _materials;
        }
        set
        {
            _materials = value;
            UpdateClipPlane(true);
        }
    }

    private Vector3 _cachedUp;
    private Vector3 _cachedPosition;
    private Vector3 _offset;

    private float _ratio = 0.5f;
    private float _ratioTMP = 0.5f;

    private Bounds _bounds;

    private AxisMode _axisMode = AxisMode.Axis_X;
    private int _sign = 1;
    
    public float Ratio
    {
        get
        {
            return _ratio;
        }
        set
        {
            _ratioTMP = value;
        }
    }

    private void Start()
    {
        //UpdateClipPlane(true);
    }

    public void UpdateClipPlane(bool forceUpdate = false)
    {
        if (Root.Instance.transform.childCount == 0)
            return;

        Transform refTransform = Root.Instance.Model.transform;

        if (_cachedPosition != refTransform.position ||
            _cachedUp != refTransform.up ||
            _ratio != _ratioTMP ||
            forceUpdate)
        {
            _bounds = (Bounds)Root.Instance.CurrentGlobalBounds;
            _cachedPosition = _bounds.center;
            _cachedUp = refTransform.up;


            Vector3 pos = _cachedPosition;

            switch (_axisMode)
            {
                case AxisMode.Axis_X:
                    {
                        pos += transform.up * (_ratioTMP * (_bounds.size.x) - _bounds.size.x * 0.5f);
                    }
                    break;
                case AxisMode.Axis_Y:
                    {
                        pos += transform.up * (_ratioTMP * (_bounds.size.y) - _bounds.size.y * 0.5f);
                    }
                    break;
                case AxisMode.Axis_Z:
                    {
                        pos += transform.up * (_ratioTMP * (_bounds.size.z) - _bounds.size.z * 0.5f);
                    }
                    break;
            }

            _ratio = _ratioTMP;

            //create plane
            Plane plane = new Plane(transform.up * _sign, pos);
            //transfer values from plane to vector4
            Vector4 planeRepresentation = new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance);
            //pass vector to shader

            foreach(Material mat in Materials)
            {
                mat.SetVector("_Plane", planeRepresentation);
            }

            transform.position = pos;
        }
    }

    public void ChangeAxis(AxisMode axis)
    {
        switch(axis)
        {
            case AxisMode.Axis_X:
                {
                    transform.forward = new Vector3(0.0f, -1.0f, 0.0f);
                    transform.up = new Vector3(1.0f, 0.0f, 0.0f);
                }
                break;
            case AxisMode.Axis_Y:
                {
                    transform.up = new Vector3(0.0f, 0.0f, 1.0f);
                    transform.forward = new Vector3(0.0f, 0.0f, 1.0f);
                }
                break;
            case AxisMode.Axis_Z:
                {
                    transform.forward = new Vector3(0.0f, -1.0f, 0.0f);
                    transform.up = new Vector3(0.0f, 0.0f, 1.0f);
                }
                break;
            default:
                {
                    throw new NotImplementedException();
                }
        }
        _axisMode = axis;
        UpdateClipPlane(true);
    }

    public void ToggleSign()
    {
        _sign = -_sign;
        UpdateClipPlane(true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(_bounds.center, _bounds.size);
        Gizmos.color = Color.red;

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawCube(transform.position, new Vector3(10.0f, 0.005f, 10.0f));
    }

    //execute every frame
    void Update()
    {
        UpdateClipPlane();

    }
}
