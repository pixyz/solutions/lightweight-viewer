﻿using System.Linq;
using UnityEngine;

public enum ExplodeOrigin {
    Zero,
    Center,
}

public class ExplodeView : MonoBehaviour
{
    private ExplodeOrigin origin = ExplodeOrigin.Zero;
    private float power = 5f;

    private Vector3[] basePositions;
    private Vector3[] baseCenters;
    private Vector3 center = new Vector3();

    public void Awake()
    {
        Root.changed.AddListener(Initialize);
    }

    public void Initialize()
    {
        // Gathers objects eligible for exploded view
        basePositions = Root.Instance.renderers.Select(x => x.transform.position).ToArray();
        baseCenters = Root.Instance.renderers.Select(x => x.bounds.center).ToArray();

        // Get center for translations
        switch (origin) {
            case ExplodeOrigin.Zero:
                center = Vector3.zero;
                break;
            case ExplodeOrigin.Center:
                center = Root.Instance.gameObject.GetChildren(true, false).GetBoundsWorldSpace().center;
                break;
        }
        // Slider may not be at 0.
        // If so, re-explode parts to make sure newly loaded parts are exploded when they arrive
        /*
        if (slider.Slider.value > 0)
            ExplodeParts();
            */
    }

    public void ExplodeParts(float value, bool explodeX, bool explodeY, bool explodeZ)
    {
        float coefficient = value;
        for (int i = 0; i < Root.Instance.renderers.Length; i++) {
            ExplodePart(i, coefficient, explodeX, explodeY, explodeZ);
        }
    }

    private void ExplodePart(int partIndex, float coefficient, bool explodeX, bool explodeY, bool explodeZ)
    {
        Renderer renderer = Root.Instance.renderers[partIndex];
        Vector3 basePosition = basePositions[partIndex];
        Vector3 direction = Vector3.Scale(baseCenters[partIndex] - center, new Vector3(explodeX ? 1 : 0, explodeY ? 1 : 0, explodeZ ? 1 : 0));
        renderer.transform.position = basePosition + power * coefficient * direction;
    }
}
