﻿using UnityEngine;

public class ThemeRefs : ProjectReferences<ThemeRefs>
{
    [Header("Accent color")]

    [SerializeField]
    private Color accent;
    public static Color Accent => Instance.accent;

    [Header("Background colors")]

    [SerializeField]
    private Color color0;
    public static Color Color0 => Instance.color0;

    [SerializeField]
    private Color color1;
    public static Color Color1 => Instance.color1;

    [SerializeField]
    private Color color2;
    public static Color Color2 => Instance.color2;

    [Header("Background color for buttons, dropdowns, ...")]

    [SerializeField]
    private Color color3;
    public static Color Color3 => Instance.color3;

    [Header("Foreground color (general purpose)")]

    [SerializeField]
    private Color text0;
    public static Color Text0 => Instance.text0;

    [Header("Foreground color for buttons, dropdowns, ...")]

    [SerializeField]
    private Color text1;
    public static Color Text1 => Instance.text1;
}