﻿using System;
using UnityEditor;
using UnityEngine;

public class PlatformSettings : ScriptableObject
{
    public PlatformSetting[] platforms = new PlatformSetting[0];

    private const string PATH = "Assets/Settings/PlatformSettings.asset";

    private static PlatformSettings instance;
    public static PlatformSettings Instance {
        get {
            if (!instance) {
                instance = AssetDatabase.LoadAssetAtPath<PlatformSettings>(PATH);
                if (instance == null) {
                    AssetDatabase.CreateAsset(instance = CreateInstance<PlatformSettings>(), PATH);
                }
            }
            return instance;
        }
    }
}

[Serializable]
public class PlatformSetting
{
    [HideInInspector]
    public string name; 

    public BuildTarget buildTarget;
    public InteractionModel[] interactionModels;

    public void SetName() {
        name = $"{buildTarget} ({interactionModels?.Length ?? 0})";
    }
}