﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class BuildPreprocessor : IPreprocessBuildWithReport, IActiveBuildTargetChanged {
    [InitializeOnLoadMethod]
    public static void SetupForBuildtarget()
    {
#if STEAM_VR
        if (EditorUserBuildSettings.selectedBuildTargetGroup != BuildTargetGroup.Standalone) {
            Valve.VR.SteamVR_Settings.instance.autoEnableVR = false;
            PlayerSettings.virtualRealitySupported = false;
        }
#endif
    }

    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildReport report)
    {
        SetupForBuildtarget();
        CheckInteractionModel(EditorUserBuildSettings.activeBuildTarget);
    }

    public void OnActiveBuildTargetChanged(BuildTarget previousTarget, BuildTarget newTarget)
    {
        CheckInteractionModel(newTarget);
    }

    public void CheckInteractionModel(BuildTarget newBuildTarget)
    {
        BuildTarget buildTarget = newBuildTarget;
        PlatformSetting supportedInteractionsForPlatform = PlatformSettings.Instance.platforms.Where(x => x.buildTarget == buildTarget).FirstOrDefault();

        if (supportedInteractionsForPlatform == null) {
            Debug.LogFormat($"Platform '{buildTarget}' is not supported in UniversalViewer.", MessageType.Error);
        } else {
            int i = 0;
            var includedInteractionModels = new List<InteractionModel>();
            foreach (InteractionModel interactionMode in supportedInteractionsForPlatform.interactionModels) {
                if (interactionMode == null) {
                    Debug.LogError("Missing InteractionModel !");
                    continue;
                }
                bool previousValue = (i == 0) || InteractionRefs.ReferencedModels.Contains(interactionMode);
                if (previousValue)
                    includedInteractionModels.Add(interactionMode);
                i++;
            }
            bool referencesChanged = false;
            for (int j = 0; j < includedInteractionModels.Count; j++) {
                if (InteractionRefs.ReferencedModels.Count != includedInteractionModels.Count) {
                    referencesChanged = true;
                    break;
                }
                if (InteractionRefs.ReferencedModels[j] != includedInteractionModels[j]) {
                    referencesChanged = true;
                    break;
                }
            }
            if (referencesChanged) {
                InteractionRefs.ReferencedModels = includedInteractionModels;
            }
        }
    }
}