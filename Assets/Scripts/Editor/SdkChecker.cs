﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

/// <summary>
/// The purpose of this class is to automatically add preprocessor defines when an SDK is available.
/// This way, code referencing a missing SDK won't produce any errors if the SDK is missing.
/// To check if a SDK is present, we can either use reflection or check if the SDK is physically present
/// in the project or on the machine depending on the nature of the SDK.
/// </summary>
public static class SdkChecker
{
    private static HashSet<string> GetDefines()
    {
        string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
        return new HashSet<string>(definesString.Split(';').Select(x => x.Trim()).Where(x => !string.IsNullOrEmpty(x)));
    }

    [InitializeOnLoadMethod]
    public static void SetupDefinesForSDKs()
    {
        HashSet<string> defines = GetDefines();

        bool changed = false;

        // Check OpenVR
        if (Type.GetType("Valve.VR.OpenVR, SteamVR") != null) {
            changed |= defines.Add("STEAM_VR");
        } else {
            changed |= defines.Remove("STEAM_VR");
        }

        if (Type.GetType("Microsoft.MixedReality.Toolkit.MixedRealityToolkit, Microsoft.MixedReality.Toolkit") != null) {
            changed |= defines.Add("MRTK_RC2");
        } else {
            changed |= defines.Remove("MRTK_RC2");
        }

        if (changed) {
            Debug.Log("Defines have been changed ! : " + string.Join(";", defines.ToArray()));
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, string.Join(";", defines.ToArray()));
        }
    }
}