﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Universal Viewer/User Interaction Model")]
public class InteractionModel : ScriptableObject
{
    [Header("Controls")]
    public Layout canvasControl;
    public LabelControl labelControl;
    public SliderControl sliderControl;
    public ToggleControl toggleControl;
    public ToggleLabelControl toggleLabelControl;
    public DropdownControl dropdownControl;
    public ButtonControl buttonControl;
    public HierarchyControl hierarchyControl;
    public HierarchyNodeControl hierarchyNodeControl;
    public MetadataControl metadataControl;

    public static UnityEvent changed = new UnityEvent();

    private static InteractionModel current;

    public static InteractionModel Current {
        get {
            if (current == null)
                current = InteractionRefs.ReferencedModels[0];
            return current;
        }
        set {
            if (current != value) {
                current = value;
                changed.Invoke();
                Debug.Log("Current Changed");
            }
        }
    }
}