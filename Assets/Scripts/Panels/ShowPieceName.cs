﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ShowPieceName : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI text;

    public string Name {
        get {
            return text.text;
        }
        set {
            text.text = value;
        }
    }
    public float distanceThreshold;

    public Vector2 positionStart;

    private Vector2 PointerPosition {
        get {
#if UNITY_STANDALONE || UNITY_EDITOR
            return Input.mousePosition;
#elif UNITY_IOS || UNITY_ANDROID
            return Input.GetTouch(0).position;
#else
            return new Vector2(0f,0f);
            //throw new NotImplementedException();
#endif
        }
    }

    private void Start()
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width / 2, GetComponent<RectTransform>().sizeDelta.y);
    }

    private void Update()
    {


#if UNITY_STANDALONE || UNITY_EDITOR

        if (!Input.GetMouseButton(0) || Vector2.Distance(positionStart, PointerPosition) > distanceThreshold) {
            Destroy(gameObject);
        }
#elif UNITY_IOS || UNITY_ANDROID
        if (Input.GetTouch(0).phase == TouchPhase.Ended || Vector2.Distance(positionStart, PointerPosition) > distanceThreshold) {
            Destroy(gameObject);
        
        }
#endif
    }
}
