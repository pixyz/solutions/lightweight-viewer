﻿﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Globalization;

public class VisualizationPanel : BasePanel
{

    private SliderControl sliderExplode;
    private SliderControl sliderClipPlane;

    private DropdownControl dropDownClipPlaneMode;

    private ToggleLabelControl explodeX;
    private ToggleLabelControl explodeY;
    private ToggleLabelControl explodeZ;

    private ToggleGroup viewGroup;

    private ToggleLabelControl wireframeView;
    private ToggleLabelControl verticesView;
    private ToggleLabelControl vertexColorsView;
    private ToggleLabelControl uvView;
    private ToggleLabelControl normalsView;
    private ToggleLabelControl densityView;
    private ToggleLabelControl clipPlaneView;

    private ExplodeView explodeView;
    
    private Toggle activeView = null;

    private VisualizationMode visualizationMode;


    public override string PanelName => "Visualization";

    public override Sprite PanelIcon => Resources.Load<Sprite>("Icons/Eye");

    void Awake()
    {
        visualizationMode = gameObject.AddComponent<VisualizationMode>();

        RefreshGUI();

        explodeView = gameObject.AddComponent<ExplodeView>();
        explodeView.Initialize();

        Root.changed.AddListener(Initialize);
    }

    void Initialize()
    {
        explodeX.Toggle.onValueChanged.RemoveAllListeners();
        explodeX.Toggle.isOn = true;
        explodeX.Toggle.onValueChanged.AddListener((b) => { explodeView.ExplodeParts(sliderExplode.Slider.value, explodeX.Toggle.isOn, explodeY.Toggle.isOn, explodeZ.Toggle.isOn); });

        explodeY.Toggle.onValueChanged.RemoveAllListeners();
        explodeY.Toggle.isOn = true;
        explodeY.Toggle.onValueChanged.AddListener((b) => { explodeView.ExplodeParts(sliderExplode.Slider.value, explodeX.Toggle.isOn, explodeY.Toggle.isOn, explodeZ.Toggle.isOn); });

        explodeZ.Toggle.onValueChanged.RemoveAllListeners();
        explodeZ.Toggle.isOn = true;
        explodeZ.Toggle.onValueChanged.AddListener((b) => { explodeView.ExplodeParts(sliderExplode.Slider.value, explodeX.Toggle.isOn, explodeY.Toggle.isOn, explodeZ.Toggle.isOn); });

        sliderExplode.Slider.onValueChanged.RemoveAllListeners();
        sliderExplode.Slider.value = 0;
        sliderExplode.Slider.onValueChanged.AddListener((v) => { explodeView.ExplodeParts(sliderExplode.Slider.value, explodeX.Toggle.isOn, explodeY.Toggle.isOn, explodeZ.Toggle.isOn);});

        wireframeView.Toggle.onValueChanged.RemoveAllListeners();
        wireframeView.Toggle.isOn = false;
        wireframeView.Toggle.onValueChanged.AddListener((b) => { visualizationMode.ChangeView(b, VisualizationModeType.Default_WireFrame); SetActiveView(wireframeView.Toggle, b); });

        verticesView.Toggle.onValueChanged.RemoveAllListeners();
        verticesView.Toggle.isOn = false;
        verticesView.Toggle.onValueChanged.AddListener((b) => { visualizationMode.ChangeView(b, VisualizationModeType.Default_Vertices); SetActiveView(verticesView.Toggle, b); });

        vertexColorsView.Toggle.onValueChanged.RemoveAllListeners();
        vertexColorsView.Toggle.isOn = false;
        vertexColorsView.Toggle.onValueChanged.AddListener((b) => { visualizationMode.ChangeView(b, VisualizationModeType.VertexColor); SetActiveView(vertexColorsView.Toggle, b); });

        uvView.Toggle.onValueChanged.RemoveAllListeners();
        uvView.Toggle.isOn = false;
        uvView.Toggle.onValueChanged.AddListener((b) => { visualizationMode.ChangeView(b, VisualizationModeType.UV); SetActiveView(uvView.Toggle, b); });

        normalsView.Toggle.onValueChanged.RemoveAllListeners();
        normalsView.Toggle.isOn = false;
        normalsView.Toggle.onValueChanged.AddListener((b) => { visualizationMode.ChangeView(b, VisualizationModeType.Normals); SetActiveView(normalsView.Toggle, b); });

        densityView.Toggle.onValueChanged.RemoveAllListeners();
        densityView.Toggle.isOn = false;
        densityView.Toggle.onValueChanged.AddListener((b) => { visualizationMode.ChangeView(b, VisualizationModeType.Density); SetActiveView(densityView.Toggle, b); });

        clipPlaneView.Toggle.onValueChanged.RemoveAllListeners();
        clipPlaneView.Toggle.isOn = false;
        clipPlaneView.Toggle.onValueChanged.AddListener((b) => { visualizationMode.ActivateClipPlane(b); });

        dropDownClipPlaneMode.Dropdown.onValueChanged.RemoveAllListeners();
        dropDownClipPlaneMode.Dropdown.value = 0;
        dropDownClipPlaneMode.Dropdown.onValueChanged.AddListener((o) => visualizationMode.ClipPlane.ChangeAxis((ClipPlane.AxisMode)o));

        sliderClipPlane.Slider.onValueChanged.RemoveAllListeners();
        sliderClipPlane.Slider.value = 0.5f;
        sliderClipPlane.Slider.onValueChanged.AddListener((v) => visualizationMode.ClipPlane.Ratio = v);
    }

    public override void RefreshGUI()
    {
        ToggleControl baseToggle = InteractionModel.Current.toggleControl;
        ToggleLabelControl labelToggle = InteractionModel.Current.toggleLabelControl;
        DropdownControl baseDropDown = InteractionModel.Current.dropdownControl;
        SliderControl baseSlider = InteractionModel.Current.sliderControl;


        explodeX = AddLabelToggle(labelToggle, "Explode X Axis", "", true);
        explodeY = AddLabelToggle(labelToggle, "Explode Y Axis", "", true);
        explodeZ = AddLabelToggle(labelToggle, "Explode Z Axis", "", true);

        sliderExplode = AddControl(baseSlider);
        sliderExplode.Name = "Distance";

        wireframeView = AddLabelToggle(labelToggle, "Triangles", intToString(Root.Instance.triangleCount), false);

        verticesView = AddLabelToggle(labelToggle, "Vertices", intToString(Root.Instance.vertexCount), false);

        vertexColorsView = AddLabelToggle(labelToggle, "Vertex Colors", Root.Instance.hasVertexColors ? "Yes" : "No", false);

        uvView = AddLabelToggle(labelToggle, "UVs", Root.Instance.hasUVs ? "Yes" : "No", false);

        normalsView = AddLabelToggle(labelToggle, "Normals", "", false);

        densityView = AddLabelToggle(labelToggle, "Density", "", false);

        clipPlaneView = AddLabelToggle(labelToggle, "Clip Plane", "", false);


        dropDownClipPlaneMode = AddControl(baseDropDown);
        dropDownClipPlaneMode.Name = "ClipPlane Mode";
        dropDownClipPlaneMode.Dropdown.options.Add(new TMPro.TMP_Dropdown.OptionData("X Axis"));
        dropDownClipPlaneMode.Dropdown.options.Add(new TMPro.TMP_Dropdown.OptionData("Y Axis"));
        dropDownClipPlaneMode.Dropdown.options.Add(new TMPro.TMP_Dropdown.OptionData("Z Axis"));
        

        sliderClipPlane = AddControl(baseSlider);
        sliderClipPlane.Name = "Translation";

        Initialize();
    }

    private ToggleLabelControl AddLabelToggle(ToggleLabelControl model, string property, string value, bool isOn)
    {
        ToggleLabelControl toggleControl = AddControl(model);
        toggleControl.Toggle.isOn = isOn;
        toggleControl.Property = property;
        toggleControl.Value = value;
        return toggleControl;
    }

    private string intToString(int n)
    {
        var f = new NumberFormatInfo { NumberGroupSeparator = " " };
        var s = n.ToString("n0", f);
        return s;
    }

    private void SetActiveView(Toggle target, bool state)
    {
        if(state)
        {
            Toggle oldView = activeView;
            activeView = null;

            if (oldView != null)
            {
                oldView.isOn = false;
            }
            else
            {
                visualizationMode.DefaultView(false);
            }
            activeView = target;
        }
        else
        {
            if (activeView != null)
            {
                visualizationMode.DefaultView(true);
                activeView = null;
            }
        }
    }
}