﻿using UnityEngine;


public class ObjectPicker : MonoBehaviour {
    public float distanceThreshold = 10;

    private Vector2 positionStart;

    private Vector2 PointerPosition
    {
        get {
#if UNITY_STANDALONE || UNITY_EDITOR
            return Input.mousePosition;
#elif UNITY_IOS || UNITY_ANDROID
            return Input.GetTouch(0).position;
#else
            return new Vector2(0f,0f);
            //throw new NotImplementedException();
#endif
        }
    }

    private void Update()
    {
        // Exit if mouse is over UI
        if (Extensions.IsDirectlyOverUI(PointerPosition)) {
            return;
        }

        Vector2 positionEnd = Vector2.zero;

#if UNITY_STANDALONE || UNITY_EDITOR
        if (Input.GetMouseButtonDown(1)) {
            positionStart = PointerPosition;
        } else if (Input.GetMouseButtonUp(1)) {
            positionEnd = PointerPosition;
        } else {
            return;
        }
#elif UNITY_IOS || UNITY_ANDROID
        if (Input.touchCount > 0) {
            if (Input.GetTouch(0).phase == TouchPhase.Began) {
                positionStart = PointerPosition;
            } else if (Input.GetTouch(0).phase == TouchPhase.Ended) {
                positionEnd = PointerPosition;
            } else {
                return;
            }
        } else {
            return;
        }
#endif

        // Exit if pointer has moved too much
        if (Vector2.Distance(positionStart, positionEnd) > distanceThreshold) {
            return;
        }

        Transform target = Raycast(PointerPosition);
        Selection.Selected = target;
    }

    private Transform Raycast(Vector2 postion)
    {
        Ray ray;
        ray = Camera.main.ScreenPointToRay(postion);
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        return hit.transform;
    }
}