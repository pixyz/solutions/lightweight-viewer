﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TriggerOnPointer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public UnityEvent pointerEntered;
    public UnityEvent pointerExited;
    public GameObject menu;

    private bool isNotOverUI;

    private void Update()
    {
        CheckIsOverUI();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        pointerEntered?.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pointerExited?.Invoke();
    }

    public void OpenMenu()
    {
        Debug.Log(isNotOverUI);
        if (isNotOverUI) {
            menu.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    private void CheckIsOverUI()
    {
        if (Input.GetMouseButtonDown(0)) {
            if (!EventSystem.current.IsPointerOverGameObject()) {
                isNotOverUI = false;
            }
        }
        if (Input.GetMouseButtonUp(0)) {
            isNotOverUI = true;
        }
        if (!Input.GetMouseButton(0)) {
            isNotOverUI = true;
        }
    }
}
