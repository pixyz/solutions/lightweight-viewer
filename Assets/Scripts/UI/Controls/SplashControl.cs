﻿using UnityEngine;
using UnityEngine.UI;

public class SplashControl : MonoBehaviour
{
    public GameObject logo;
    public Text text;

    private float minSpeed = 0;
    public float maxSpeed = 40.0f;
    private float t = 0.0f;

    private bool rotate;

    void FixedUpdate()
    {
        if (rotate) {
            logo?.transform.Rotate(Vector3.back, Mathf.Lerp(minSpeed, maxSpeed, t));// * Time.deltaTime));
            //speed = maxSpeed;
            t += 1.0f * Time.deltaTime;
            if (t > 0.9f) {
                float temp = minSpeed;
                minSpeed = maxSpeed;
                maxSpeed = temp;
                t = 0f;
            }
        }
    }

    public void RotateLogo()
    {
        rotate = true;
    }

    public void StopRotateLogo()
    {
        rotate = false;
        minSpeed = 0;
        maxSpeed = 20.0f;
    }
}
