﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class HierarchyExplorerNodeControl : HierarchyNodeControl
{
    public UnityEvent longPressed;
    public Button Button => button;
    public  GameObject bigText; // For Instantiate
    public Transform myObject;

    private GameObject myBigText; // For Use
    private readonly float distanceThreshold = 50;
    private bool isNeo;
    private float timeclicked;

    [SerializeField]
    private TextMeshProUGUI labelName;
    [SerializeField]
    private Button button;
    [SerializeField]
    private float TIME_TO_APPEAR;

    private RectTransform rectTransform;
    private Vector2 positionStart;
    private Vector2 PointerPosition {
        get {
#if UNITY_STANDALONE || UNITY_EDITOR
            return Input.mousePosition;
#elif UNITY_IOS || UNITY_ANDROID
            return Input.GetTouch(0).position;
#else
            return new Vector2(0f,0f);
            //throw new NotImplementedException();
#endif
        }
    }

    public string Name {
        get {
            return labelName.text;
        }
        set {
            labelName.text = value;
        }
    }

    private void Start()
    {
        rectTransform = GetComponent < RectTransform>();
        Selection.changed.AddListener(SelectedChanged);
        SelectedChanged();
    }

    private void SelectedChanged()
    {
#if MRTK_RC2

#elif UNITY_IOS || UNITY_ANDROID || UNITY_STANDALONE || UNITY_EDITOR
        //if (myObject == Selection.Selected || transform.parent.parent.parent.GetComponent<AndroidHierarchyPanel>().parents.Contains(myObject)) {//ooook
        //    transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = ThemeRefs.Accent;
        //} else {
        //    transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = Color.white;
        //}
#endif

    }
    public void PressButton()
    {
        transform.GetComponentInParent<HierarchyExplorerControl>().Cible = myObject;
    }
    private void Update()
    {
#if UNITY_STANDALONE || UNITY_EDITOR
        
        if (Input.GetMouseButtonDown(0) && IsPointerInside()) {
            positionStart = PointerPosition;
            timeclicked = Time.time;
            isNeo = true;
        }

#elif UNITY_IOS || UNITY_ANDROID

        if (Input.GetTouch(0).phase == TouchPhase.Began && IsPointerInside()) {
            positionStart = PointerPosition;
            timeclicked = Time.time;
            isNeo = true;
        }

#endif



#if UNITY_STANDALONE || UNITY_EDITOR

        if (Input.GetMouseButton(0) &&
            timeclicked < Time.time - TIME_TO_APPEAR &&
            IsPointerInside() &&
            isNeo &&
            Vector2.Distance(positionStart, PointerPosition) < distanceThreshold) {
            Selection.Selected = myObject;
            if (!myBigText) {
                myBigText = Instantiate(bigText, new Vector2(Screen.width / 2, Screen.height * 0.95f), Quaternion.identity, transform.parent.parent.parent);
                myBigText.GetComponent<ShowPieceName>().positionStart = positionStart;
                myBigText.GetComponent<ShowPieceName>().Name = labelName.text;
                myBigText.GetComponent<ShowPieceName>().distanceThreshold = distanceThreshold;
            }
            isNeo = false;
        }


#elif UNITY_IOS || UNITY_ANDROID

            if ((Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary) &&
            timeclicked < Time.time - TIME_TO_APPEAR &&
            IsPointerInside() &&
            isNeo &&
            Vector2.Distance(positionStart, PointerPosition) < distanceThreshold) {
        Selection.Selected = myObject;
            if (!myBigText) {
                myBigText = Instantiate(bigText, new Vector2(Screen.width / 2, Screen.height * 0.95f ), Quaternion.identity, transform.parent.parent.parent);
                myBigText.GetComponent<ShowPieceName>().positionStart = positionStart;
                myBigText.GetComponent<ShowPieceName>().Name = labelName.text;
                myBigText.GetComponent<ShowPieceName>().distanceThreshold = distanceThreshold;
            }
            isNeo = false;
        }

#endif

#if UNITY_STANDALONE || UNITY_EDITOR
        if (Input.GetMouseButtonUp(0)&&
            timeclicked > Time.time - TIME_TO_APPEAR &&
            IsPointerInside() &&
            isNeo &&
            Vector2.Distance(positionStart, PointerPosition) < distanceThreshold) {

            if (myObject.transform.childCount > 0) {
                transform.GetComponentInParent<HierarchyExplorerControl>().Cible = myObject;
            }
            isNeo = false;
        }
#elif UNITY_IOS || UNITY_ANDROID
        if (Input.GetTouch(0).phase == TouchPhase.Ended &&
            timeclicked > Time.time - TIME_TO_APPEAR &&
            IsPointerInside() &&
            isNeo &&
            Vector2.Distance(positionStart, PointerPosition) < distanceThreshold) {
        if (myObject.transform.childCount > 0) {
            transform.GetComponentInParent<HierarchyExplorerControl>().Cible = myObject;
        }
        isNeo = false;

        }
#endif



    }

    public bool IsPointerInside()
    {
        Vector2 localMousePosition = rectTransform.InverseTransformPoint(Input.mousePosition);
        return (rectTransform.rect.Contains(localMousePosition));
    }

}
