﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HierarchyExplorerControl : HierarchyControl
{
    public GameObject content;

    private static Transform cible;
    public List<Transform> parents = new List<Transform>();
    private List<HierarchyExplorerNodeControl> children = new List<HierarchyExplorerNodeControl>();

    private int branch;

    public Transform Cible {
        set {
            if(cible != value) {
                cible = value;
                if(cible.childCount > 0)
                ShowHierachy();
            }
        }
    }

    private void Start()
    {
        Selection.changed.AddListener(SelectedChanged);
        SelectedChanged();

        if (Root.Instance.transform.childCount > 0 && Root.Instance.transform.GetChild(0) != null) {
            cible = Root.Instance.transform.GetChild(0);
            ShowHierachy();
        }
    }

    private void SelectedChanged()
    {
        if (Selection.Selected != null) {
            cible = Selection.Selected.parent;
        }
        GetSelectedParent(Selection.Selected);
    }

    private void ShowHierachy()
    {
        foreach (var hierarchyNode in children) { 
            if (hierarchyNode)
                Destroy(hierarchyNode.gameObject);
        }
        children.Clear();

        AddParentControl(cible);

        branch = 0;
        foreach (Transform child in cible) {

            var _child = Instantiate(nodeControl) as HierarchyExplorerNodeControl;// AddControl(clickableTextControl);
            _child.transform.SetParent(content.transform);
           

            _child.transform.rotation = new Quaternion(0,0,0,0);
            _child.GetComponent<RectTransform>().localPosition = Vector3.zero;
            _child.GetComponent<RectTransform>().localScale = Vector3.one;
            _child.GetComponent<HierarchyExplorerNodeControl>().myObject = child;

            _child.Name = ((child.childCount > 0) ? "► " : "  ") + child.name;

            children.Add(_child);
        }
        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, children.Count * nodeControl.GetComponent<RectTransform>().sizeDelta.y);
    }


    private void AddParentControl(Transform child)
    {
        if (child != Root.Instance.transform) {

            AddParentControl(child.parent);

            var parent = Instantiate(nodeControl) as HierarchyExplorerNodeControl; //AddControl(clickableTextControl);
            parent.transform.SetParent(content.transform);
            parent.transform.rotation = new Quaternion(0, 0, 0, 0);
            parent.GetComponent<RectTransform>().localPosition = Vector3.zero;
            parent.GetComponent<RectTransform>().localScale = Vector3.one;
            parent.gameObject.GetComponent<Image>().color = ThemeRefs.Color2;
            parent.Name = new string(' ', branch - 1) + "▼ " + child.name;
            parent.GetComponent<HierarchyExplorerNodeControl>().myObject = child;
            if (child.parent != null) {
                parent.Button.onClick.AddListener(() => cible = child);
                parent.Button.onClick.AddListener(() => ShowHierachy());
            }
            children.Add(parent);
        }
        branch++;
    }

    private void GetSelectedParent(Transform parent)
    {
        parents.Clear();

        if (parent != Root.Instance.transform && parent != null) {
            GetSelectedParent(parent.parent);
            parents.Add(parent);
        }
    }    
}
