﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class HierarchyControl : MonoBehaviour
{
    public HierarchyNodeControl nodeControl;

    private void Awake()
    {
        nodeControl = InteractionModel.Current.hierarchyNodeControl;
    }
}
