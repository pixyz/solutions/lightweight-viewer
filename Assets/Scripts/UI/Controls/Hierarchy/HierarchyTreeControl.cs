﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HierarchyTreeControl : HierarchyControl {

    public GameObject content;

    [HideInInspector]
    public Map<GameObject, HierarchyTreeNode> nodes = new Map<GameObject, HierarchyTreeNode>();

    public UnityEvent allCollapsed = new UnityEvent();

    private void Awake()
    {
        Root.changed.AddListener(AddTree);
        AddTree();
    }

    private void AddTree()
    {
        AddTree(Root.Instance.transform);
    }

    private void Start()
    {
        //allCollapsed.Invoke();
    }

    private void ClearNodes()
    {
        HierarchyTreeNode[] uiElements = nodes.GetValues2();
        for (int i = 0; i < uiElements.Length; i++) {
            DestroyImmediate(uiElements[i].gameObject);
        }
        nodes.Clear();
    }

    public void AddTree(Transform current)
    {
        //ClearNodes();
        if (current == null) return;
        AddNode(current);
    }

    public void AddNode(Transform transform)
    {
        if (nodes.Contains1(transform.gameObject)) return;
        HierarchyTreeNode newNode = Instantiate(InteractionModel.Current.hierarchyNodeControl) as HierarchyTreeNode;
        newNode.hierarchyControl = this;
        Transform uiParent;
        if (transform.parent && nodes.Contains1(transform.parent.gameObject)) {
            uiParent = nodes.Get1(transform.parent.gameObject).transform;
        } else {
            // Init first node
            uiParent = content.GetComponent<RectTransform>();
            ContentSizeFitter csf = newNode.gameObject.AddComponent<ContentSizeFitter>();
            csf.verticalFit = ContentSizeFitter.FitMode.MinSize;
            RectTransform rt = GetComponent<RectTransform>();
            rt.anchoredPosition = uiParent.position;
            rt.anchorMin = new Vector2(0, 1);
            rt.anchorMax = new Vector2(1, 1);
            rt.pivot = new Vector2(0f, 1f);
            rt.sizeDelta = uiParent.GetComponent<RectTransform>().rect.size;
            newNode.GetComponent<VerticalLayoutGroup>().padding = new RectOffset();
        }
        newNode.transform.SetParent(uiParent);
        nodes.Add(transform.gameObject, newNode);

        newNode.PartName = transform.name;
    }

        
}
