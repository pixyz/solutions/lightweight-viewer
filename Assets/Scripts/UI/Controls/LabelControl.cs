﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LabelControl : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI labelName;

    [SerializeField]
    private TextMeshProUGUI labelValue; 

    public string Name {
        get {
            return labelName.text;
        }
        set {
            labelName.text = value;
        }
    }

    public string Value {
        get {
            return labelValue.text;
        }
        set {
            labelValue.text = value;
        }
    }

    private bool compact = false;
    public bool Compact {
        get {
            return compact;
        }
        set {
            compact = value;
            var contentSizeFitter = GetComponent<ContentSizeFitter>();
            if (compact) {

            } else {

            }
        }
    }
}
