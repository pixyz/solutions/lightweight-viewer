﻿using UnityEngine;

public class Container : MonoBehaviour
{
    [SerializeField]
    private RectTransform content;

    public RectTransform Content => content;
}