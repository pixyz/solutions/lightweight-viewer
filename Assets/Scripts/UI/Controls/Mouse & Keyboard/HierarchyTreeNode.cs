﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HierarchyTreeNode : HierarchyNodeControl {

    public HierarchyTreeControl hierarchyControl { get; set; }

    [SerializeField]
    private TextMeshProUGUI text;

    [SerializeField]
    private Button expandButton;

    [SerializeField]
    private Button selectButton;

    private bool personalCollapse;
    
    private void Start()
    {
        expandButton.onClick.AddListener(() => ToggleCollapsed());
        selectButton.onClick.AddListener(() => Selection.Selected = hierarchyControl.nodes.Get2(this).transform);
        Collapsed = true;

        Selection.changed.AddListener(SelectionChanged);
        //hierarchyControl.allCollapsed.AddListener(HierarchyCollapsed);
        expandButton.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        if (hierarchyControl.nodes.Get2(this).transform.childCount > 0)
            expandButton.GetComponent<Image>().color = Color.white;
    }

    public string PartName{
        get {
            return text.text;
        }
        set {
            text.text = value;
        }
    }

    private bool collapsed;
    public bool Collapsed
    {
        get {
            return collapsed;
        }
        set {

            if (collapsed != value) {
                collapsed = value;
                ArrowChange();
                foreach (HierarchyTreeNode child in gameObject.GetComponentsInChildren<HierarchyTreeNode>(true)) {
                    if (child.transform.parent == this.transform) {
                        child.gameObject.SetActive(!collapsed);
                    }
                }
            }
        }
    }

    private void HierarchyCollapsed()
    {
        Collapsed = true;
    }

    public void ToggleCollapsed()
    {
        Collapsed = !Collapsed;
        if (!Collapsed)
        {
            foreach (Transform child in hierarchyControl.nodes.Get2(this).transform)
            {
                hierarchyControl.AddTree(child);
            }
            
        }
    }

    private void ArrowChange()
    {
        if (hierarchyControl.nodes.Get2(this).transform.childCount > 0) {
            if (Collapsed) {
                expandButton.image.sprite = ImageRefs.UncollapseButtonIcon;
            } else {
                expandButton.image.sprite = ImageRefs.CollapseButtonIcon;
            }
        }
    }

    private void SelectionChanged()
    {
        if (Selection.Selected != null) {
            if (Selection.Selected.transform == hierarchyControl.nodes.Get2(this).transform) {
                text.color = ThemeRefs.Accent; // other color ?
            } else if(HaveInChild(hierarchyControl.nodes.Get2(this).transform, Selection.Selected.transform)) {
                text.color = ThemeRefs.Accent;
                //Collapsed = false;
            } else {
                text.color = Color.white;
            }
        } else {
            text.color = Color.white;
        }
    }

    private bool HaveInChild(Transform transform , Transform Child)
    {
        Transform[] childs = transform.gameObject.GetComponentsInChildren<Transform>();
        foreach(Transform child in childs) {
            if (child == Child) {
                return true;
            }
        }
        return false;
    }
    
}   