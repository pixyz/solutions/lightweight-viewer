﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToggleControl : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI labelName;

    [SerializeField]
    private Toggle toggle;

    public string Name {
        get {
            return labelName.text;
        }
        set {
            labelName.text = value;
        }
    }

    public Toggle Toggle => toggle;
}
