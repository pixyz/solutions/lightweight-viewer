﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nanolabo {

    public enum MovementType {
        Linear,
        SmoothStep,
        EaseIn,
        Sprint,
        Bounce,
        Overstep,
        Custom,
    }

    public static class Tweening {

        public static float duration = 0.3f;

        public static MovementType movementType = MovementType.EaseIn;

        public static float power = 0.5f;

        public static AnimationCurve custom;

        public static bool waitForEnd = true;

        private static HashSet<RectTransform> movingTransforms = new HashSet<RectTransform>();

        public static void HideToLeft(RectTransform target) {
            Vector2 start = new Vector2(0.5f * Screen.width, target.position.y);
            Vector2 end = new Vector2(-0.5f * Screen.width, target.position.y);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration, () => target.gameObject.SetActive(false)));
        }

        public static void HideToRight(RectTransform target) {
            Vector2 start = new Vector2(0.5f * Screen.width, target.position.y);
            Vector2 end = new Vector2(1.5f * Screen.width, target.position.y);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration, () => target.gameObject.SetActive(false)));
        }

        public static void HideToTop(RectTransform target) {
            Vector2 start = new Vector2(target.position.x, 0.5f * Screen.height);
            Vector2 end = new Vector2(target.position.x, 1.5f * Screen.height);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration, () => target.gameObject.SetActive(false)));
        }

        public static void HideToBottom(RectTransform target) {
            Vector2 start = new Vector2(target.position.x, 0.5f * Screen.height);
            Vector2 end = new Vector2(target.position.x, -0.5f * Screen.height);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration, () => target.gameObject.SetActive(false)));
        }

        public static void ShowFromLeft(RectTransform target) {
            Vector2 start = new Vector2(-0.5f * Screen.width, target.position.y);
            Vector2 end = new Vector2(0.5f * Screen.width, target.position.y);
            target.gameObject.SetActive(true);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration));
        }

        public static void ShowFromRight(RectTransform target) {
            Vector2 start = new Vector2(1.5f * Screen.width, target.position.y);
            Vector2 end = new Vector2(0.5f * Screen.width, target.position.y);
            target.gameObject.SetActive(true);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration));
        }

        public static void ShowFromTop(RectTransform target) {
            Vector2 start = new Vector2(target.position.x, 1.5f * Screen.height);
            Vector2 end = new Vector2(target.position.x, 0.5f * Screen.height);
            target.gameObject.SetActive(true);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration));
        }

        public static void ShowFromBottom(RectTransform target) {
            Vector2 start = new Vector2(target.position.x, -0.5f * Screen.height);
            Vector2 end = new Vector2(target.position.x, 0.5f * Screen.height);
            target.gameObject.SetActive(true);
            MonoContext.Instance.StartCoroutine(MoveCoroutine(target, start, end, duration));
        }

        private static float Interpolate(float start, float end, float value) {
            switch (movementType) {
                case MovementType.Linear:
                    return Mathf.Lerp(start, end, value);
                case MovementType.SmoothStep:
                    return Mathf.Lerp(start, end, Shaping.SmoothStep(value, power));
                case MovementType.EaseIn:
                    return Mathf.Lerp(start, end, Shaping.EaseIn(value, power));
                case MovementType.Sprint:
                    return Mathf.Lerp(start, end, Shaping.Sprint(value, power));
                case MovementType.Bounce:
                    return Mathf.Lerp(start, end, Shaping.Bounce(value));
                case MovementType.Overstep:
                    return Shaping.Lerp(start, end, Shaping.Overstep(value, power));
                case MovementType.Custom:
                    return Shaping.Lerp(start, end, custom.Evaluate(value));
            }
            return 0f;
        }

        private static IEnumerator MoveCoroutine(RectTransform target, Vector2 start, Vector2 end, float duration, Action doAfter = null) {
            if (waitForEnd && movingTransforms.Contains(target))
                yield break;
            movingTransforms.Add(target);
            for (float elapsed = 0f; elapsed < duration; elapsed += Time.deltaTime) {
                target.position = new Vector3(Interpolate(start.x, end.x, elapsed / duration), Interpolate(start.y, end.y, elapsed / duration));
                yield return null;
            }
            target.position = end;
            doAfter?.Invoke();
            movingTransforms.Remove(target);
        }
    }
}