﻿using System.Collections.Generic;
using UnityEngine;

public class UIBuilder : MonoBehaviour
{
    void Start()
    {
        RebuildUI();
        InteractionModel.changed.AddListener(RebuildUI);
    }

    public void RebuildUI() {

        // Delete children
        List<GameObject> children = gameObject.GetChildren(true, false);
        foreach (GameObject child in children) {
            DestroyImmediate(child.gameObject);
        }

        // Build UI
        Layout canvas = Instantiate(InteractionModel.Current.canvasControl);
        canvas.transform.SetParent(transform);
        canvas.transform.localScale = Vector3.one;
    }
}
