﻿using UnityEngine;

public class Autocenter : MonoBehaviour
{
    private Bounds totalBounds;

    public void Center()
    {
        Bounds? nbounds = transform.GetTreeBounds();

        if (nbounds == null)
            return;

        totalBounds = (Bounds)nbounds;

        Vector3 worldbounds = totalBounds.center;

        foreach (Transform grandChild in transform)
            grandChild.Translate(-worldbounds, transform);
    }
}

