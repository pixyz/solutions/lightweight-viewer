﻿using UnityEngine;
using UnityEngine.Events;


public static class Selection {
    public static UnityEvent changed = new UnityEvent();

    private static Transform selected;

    public static Transform Selected
    {
        get {
            return selected;
        }
        set {
            if (selected != value) {
                selected = value;
                changed?.Invoke();
            }
        }
    }
}
