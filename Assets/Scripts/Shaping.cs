﻿using UnityEngine;

public sealed class Shaping {

    public static float EaseIn(float x, float power) {
        power = Mathf.Clamp(power, 0.0001f, 0.9999f);
        return Mathf.Pow(Mathf.Sin(x * Mathf.PI * 0.5f), 1 - power);
    }
 
    public static float Sprint(float x, float power) {
        power = Mathf.Clamp(power, 0.0001f, 0.9999f);
        x = Mathf.Clamp(x, 0, 1);
        return Mathf.Pow(1.0f - Mathf.Cos(x * Mathf.PI * 0.5f), 1 - power);
    }

    public static float Overstep(float x, float power) {
        power = Mathf.Clamp(power, 0.0001f, 0.9999f);
        x = Mathf.Clamp(x, 0, 1);
        float e = (1 - power) / 5;
        float sqe = Mathf.Sqrt(1 - e * e);
        return 1 - Mathf.Exp(-10 * Mathf.Pow(e, 0.1f) * x) * Mathf.Sin(x * (1 / e) * sqe + Mathf.Atan(sqe / e)) / sqe;
    }

    public static float SmoothStep(float x, float power) {
        power = Mathf.Clamp(power, 0.0001f, 0.9999f);
        x = Mathf.Clamp(x, 0, 1);
        float h = Mathf.Floor(x + 0.5f);
        float g = 0.5f * Mathf.Pow(2 * (0.5F - Mathf.Abs(x - 0.5f)), 1 / (1 - power));
        return (1 - h) * g + h * (1 - g);
    }

    public static float Bounce(float x) {
        //power = Mathf.Clamp(power, 0, 1);
        x = Mathf.Clamp(x, 0, 1);
        return 1 - (1 - x) * Mathf.Abs(Mathf.Sin(4.7f * (x + 1) * (x + 1)) * (1 - x));
    }

    public static float Lerp(float start, float end, float x) {
        return start * (1 - x) + end * x;
    }
}