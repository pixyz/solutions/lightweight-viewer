﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Root : MonoBehaviour
{
    private static Root instance;
    public static Root Instance { get { return instance;} }

    public static UnityEvent changed = new UnityEvent();

    private Bounds? boundsOriginal;
    public Bounds? OriginalBounds { get { return boundsOriginal; } }

    private GameObject _model;
    public GameObject Model { get { return _model; } set { _model = value; } }

    private MaterialBank materialBank;
    private Autocenter autoCenter;

    public Renderer[] renderers { get; private set; } = new Renderer[0];
    public int triangleCount { get; private set; } = 0;
    public int vertexCount { get; private set; } = 0;
    public bool hasUVs { get; private set; } = false;
    public bool hasVertexColors { get; private set; } = false;

    public MaterialBank MaterialBank { get { return materialBank; } }
    public Bounds? CurrentGlobalBounds { get { return transform.GetTreeBounds(); } }

    void Awake()
    {
        instance = this;
        materialBank = new MaterialBank();
        autoCenter = GetComponent<Autocenter>();
        if (transform.childCount > 0) SetModel(transform.GetChild(0).gameObject);
        else if (FindObjectOfType<Pixyz.Import.ImportStamp>() != null) {
            SetModel(FindObjectOfType<Pixyz.Import.ImportStamp>().gameObject);
        }
    }


    public void SetModel(GameObject model)
    {
        DestroyModel();
        model.transform.parent = instance.transform;
        instance.Model = model;

        Initialize();
        changed.Invoke();
    }

    public void DestroyModel()
    {
        if (Model != null)
            Destroy(Instance.Model);
    }

    private void Initialize()
    {
        renderers = Model.GetChildren(true, false).GetRenderers();
        triangleCount = 0;
        vertexCount = 0;
        hasUVs = false;
        hasVertexColors = false;

        foreach (var renderer in renderers) {
            var collider = renderer.gameObject.GetComponent<MeshCollider>() ? null : renderer.gameObject.AddComponent<MeshCollider>();
            var meshFilter = renderer.GetComponent<MeshFilter>();
            if (meshFilter == null) continue;
            triangleCount += meshFilter.sharedMesh.triangles.Length;
            vertexCount += meshFilter.sharedMesh.vertexCount;
            if (!hasUVs && meshFilter.sharedMesh.uv.Length > 0) hasUVs = true;
            if (!hasVertexColors && meshFilter.sharedMesh.colors.Length > 0) hasVertexColors = true;
        }

        instance.boundsOriginal = Model.transform.GetTreeBounds();
        instance.materialBank.Initialize(Model.transform);

        Instance.autoCenter.Center();
    }
}