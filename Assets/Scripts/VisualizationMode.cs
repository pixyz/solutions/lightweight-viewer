﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VisualizationModeType
{
    Default,
    Normals,
    Default_Vertices,
    VertexColor,
    Density,
    Default_WireFrame,
    UV
}

public class VisualizationMode : MonoBehaviour
{
    private VisualizationModeType currentType = VisualizationModeType.Default;
    private bool clipPlaneStatus = false;

    //private Renderer[] renderers;

    private Material uvsMaterial;
    private Material UVsMaterial {
        get {
            if (uvsMaterial == null) {
                uvsMaterial = new Material(Shader.Find("PiXYZ/UV Overlay"));
                uvsMaterial.SetTexture(Shader.PropertyToID("_UVTex"), ImageRefs.UvTexture);
            }
            return uvsMaterial;
        }
    }
    private ClipPlane clipPlane = null;

    private Dictionary<VisualizationModeType, Material> visualizationMaterials;
    
    private struct FadeStructure {
        string parameterID;
        float minValue;
        float maxValue;
        float time;
    }

    public ClipPlane ClipPlane { get { return clipPlane; } }

    private void Awake()
    {
        GameObject clipPlaneObject = new GameObject("ClipPlane", new Type[] { typeof(ClipPlane) });
        clipPlane = clipPlaneObject.GetComponent<ClipPlane>();
        clipPlane.Materials = new List<Material>();
        clipPlane.ChangeAxis(ClipPlane.AxisMode.Axis_X);

        clipPlaneObject.SetActive(false);

        visualizationMaterials = new Dictionary<VisualizationModeType, Material>();
        visualizationMaterials.Add(VisualizationModeType.Density, Root.Instance.MaterialBank.DensityMaterial);
        visualizationMaterials.Add(VisualizationModeType.Normals, Root.Instance.MaterialBank.NormalsMaterial);
        visualizationMaterials.Add(VisualizationModeType.UV, Root.Instance.MaterialBank.UVsMaterial);
        visualizationMaterials.Add(VisualizationModeType.VertexColor, Root.Instance.MaterialBank.VertexColorMaterial);
        visualizationMaterials.Add(VisualizationModeType.Default_Vertices, Root.Instance.MaterialBank.VerticesMaterial);
        visualizationMaterials.Add(VisualizationModeType.Default_WireFrame, Root.Instance.MaterialBank.WireframeMaterial);
    }

    private void Start()
    {
        // Get root object and rendered objects
        Initialize();
        Root.changed.AddListener(Initialize);
    }

    private void Initialize()
    {
        //renderers = Root.Instance.renderers;
    }

    public void ChangeView(bool state, VisualizationModeType viewType)
    {
        visualizationMaterials[viewType].SetFloat("_clipPlaneEnabled", clipPlaneStatus ? 1.0f : 0.0f);

        if (viewType == VisualizationModeType.Default_Vertices)
        {
            ChangeVerticesView(state);
            return;
        }
        else if(viewType == VisualizationModeType.Default_WireFrame)
        {
            ChangeWireframeView(state);
            return;
        }

        if (state)
        {
            AddMaterialOnRenderers(visualizationMaterials[viewType]);
            SmoothStep((x) =>
            {
                visualizationMaterials[viewType].SetFloat("_Transparency", x);
            }, 0f, 1f, 0.25f);

            currentType = viewType;

            if (clipPlaneStatus)
            {
                clipPlane.Materials.Add(visualizationMaterials[viewType]);
                clipPlane.UpdateClipPlane(true);
            }
        }
        else
        {
            clipPlane.Materials.Remove(visualizationMaterials[viewType]);
            SmoothStep((x) => visualizationMaterials[viewType].SetFloat("_Transparency", x), 0.0025f, 0f, 0.25f, () => RemoveMaterialOnRenderers(visualizationMaterials[viewType]));
        }
    }

    public void ChangeVerticesView(bool state)
    {
        if (state)
        {
            AddMaterialOnRenderers(visualizationMaterials[VisualizationModeType.Default_Vertices]);

            DefaultView(true, true);

            SmoothStep((x) =>
            {
                visualizationMaterials[VisualizationModeType.Default_Vertices].SetFloat("_PointSize", x);
            }, 0f, 0.0025f, 0.25f);

            currentType = VisualizationModeType.Default_Vertices;

            if (clipPlaneStatus)
            {
                clipPlane.Materials.Add(visualizationMaterials[VisualizationModeType.Default_Vertices]);
                clipPlane.UpdateClipPlane(true);
            }
        }
        else
        {
            DefaultView(false);
            clipPlane.Materials.Remove(visualizationMaterials[VisualizationModeType.Default_Vertices]);
            SmoothStep((x) => visualizationMaterials[VisualizationModeType.Default_Vertices].SetFloat("_PointSize", x), 0.0025f, 0f, 0.25f, () => RemoveMaterialOnRenderers(visualizationMaterials[VisualizationModeType.Default_Vertices]));
        }
    }

    public void ChangeWireframeView(bool state)
    {
        if (state)
        {
            AddMaterialOnRenderers(visualizationMaterials[VisualizationModeType.Default_WireFrame]);

            DefaultView(true, true);

            SmoothStep((x) =>
            {
                visualizationMaterials[VisualizationModeType.Default_WireFrame].SetFloat("_Transparency", x);
            }, 0f, 1f, 0.25f);

            currentType = VisualizationModeType.Default_WireFrame;

            if (clipPlaneStatus)
            {
                clipPlane.Materials.Add(visualizationMaterials[VisualizationModeType.Default_WireFrame]);
                clipPlane.UpdateClipPlane(true);
            }
        }
        else
        {
            DefaultView(false);
            clipPlane.Materials.Remove(visualizationMaterials[VisualizationModeType.Default_WireFrame]);
            SmoothStep((x) => visualizationMaterials[VisualizationModeType.Default_WireFrame].SetFloat("_Transparency", x), 0.0025f, 0f, 0.25f, () => RemoveMaterialOnRenderers(visualizationMaterials[VisualizationModeType.Default_WireFrame]));
        }
    }


    public void DefaultView(bool state, bool keepCurrentType = false)
    {
        if (state)
        {                        
            if (clipPlaneStatus)
            {
                HashSet<Material> materials = new HashSet<Material>();
                foreach (MaterialData data in Root.Instance.MaterialBank.MaterialsData)
                {
                    List<MaterialDataVariant> variants = data.variants[MaterialType.ClipPlane];

                    for (int i = 0; i < variants.Count; ++i)
                    {
                        data.renderer.AddMaterial(variants[i].material);
                        materials.Add(variants[i].material);
                    }
                }

                clipPlane.Materials.AddRange(materials);
                clipPlane.UpdateClipPlane(true);
            }
            else
            {
                foreach (MaterialData data in Root.Instance.MaterialBank.MaterialsData)
                {
                    List<MaterialDataVariant> variants = data.variants[MaterialType.Default];

                    for (int i = 0; i < variants.Count; ++i)
                    {
                        data.renderer.AddMaterial(variants[i].material);
                    }
                }
            }

            if(!keepCurrentType)
                currentType = VisualizationModeType.Default;
        }
        else
        {
            if (currentType == VisualizationModeType.Default_Vertices || currentType == VisualizationModeType.Default_WireFrame)
                return;

            if (clipPlaneStatus)
            {
                foreach (MaterialData data in Root.Instance.MaterialBank.MaterialsData)
                {
                    List<MaterialDataVariant> variants = data.variants[MaterialType.ClipPlane];

                    foreach (MaterialDataVariant matVariant in data.variants[MaterialType.ClipPlane])
                    {                        
                        SmoothStep((x) => { }, 1f, 0f, 0.25f, () =>
                        {
                            RemoveMaterialOnRenderers(matVariant.material);
                            clipPlane.Materials.Remove(matVariant.material);
                        });
                    }
                }
            }
            else
            {
                foreach (MaterialData data in Root.Instance.MaterialBank.MaterialsData)
                {
                    List<MaterialDataVariant> variants = data.variants[MaterialType.Default];

                    foreach (MaterialDataVariant matVariant in data.variants[MaterialType.Default])
                    {
                        SmoothStep((x) => { }, 1f, 0f, 0.25f, () => RemoveMaterialOnRenderers(matVariant.material));
                    }
                }
            }
        }
    }

    public void ActivateClipPlane(bool state)
    {
        if(state)
        {
            if (currentType != VisualizationModeType.Default &&
                currentType != VisualizationModeType.Default_WireFrame &&
                currentType != VisualizationModeType.Default_Vertices)
            {
                clipPlane.Materials.Clear();
                clipPlane.Materials.Add(visualizationMaterials[currentType]);
                clipPlane.UpdateClipPlane(true);

                visualizationMaterials[currentType].SetFloat("_clipPlaneEnabled", 1.0f);
            }
            else
            {
                HashSet<Material> materials = new HashSet<Material>();
                foreach(MaterialData data in Root.Instance.MaterialBank.MaterialsData)
                {
                    List<MaterialDataVariant> variants = data.variants[MaterialType.ClipPlane];

                    for(int i = 0; i < variants.Count; ++i)
                    {
                        data.renderer.AddMaterial(variants[i].material);
                        materials.Add(variants[i].material);
                    }

                    List<MaterialDataVariant> variantsDefault = data.variants[MaterialType.Default];

                    for (int i = 0; i < variantsDefault.Count; ++i)
                    {
                        data.renderer.RemoveMaterial(variantsDefault[i].material);
                    }
                }

                clipPlane.Materials.Clear();

                if (currentType == VisualizationModeType.Default_WireFrame || currentType == VisualizationModeType.Default_Vertices)
                {
                    clipPlane.Materials.Add(visualizationMaterials[currentType]);
                    visualizationMaterials[currentType].SetFloat("_clipPlaneEnabled", 1.0f);
                }

                clipPlane.Materials.AddRange(materials);
                clipPlane.UpdateClipPlane(true);
            }
        }
        else
        {
            if (currentType != VisualizationModeType.Default &&
                currentType != VisualizationModeType.Default_WireFrame &&
                currentType != VisualizationModeType.Default_Vertices)
            {
                clipPlane.Materials.Remove(visualizationMaterials[currentType]);
                visualizationMaterials[currentType].SetFloat("_clipPlaneEnabled", 0.0f);
            }
            else
            {
                if (currentType == VisualizationModeType.Default_WireFrame || currentType == VisualizationModeType.Default_Vertices)
                {
                    clipPlane.Materials.Remove(visualizationMaterials[currentType]);
                    visualizationMaterials[currentType].SetFloat("_clipPlaneEnabled", 0.0f);
                }

                foreach (MaterialData data in Root.Instance.MaterialBank.MaterialsData)
                {
                    List<MaterialDataVariant> variantsDefault = data.variants[MaterialType.Default];

                    for (int i = 0; i < variantsDefault.Count; ++i)
                    {
                        data.renderer.AddMaterial(variantsDefault[i].material);
                    }

                    List<MaterialDataVariant> variantsClipPlane = data.variants[MaterialType.ClipPlane];

                    for (int i = 0; i < variantsDefault.Count; ++i)
                    {
                        data.renderer.RemoveMaterial(variantsClipPlane[i].material);
                    }
                }

                clipPlane.Materials.Clear();
            }

        }
        clipPlane.gameObject.SetActive(state);
        clipPlaneStatus = state;
    }

    private void AddMaterialOnRenderers(Material material)
    {
        foreach(MaterialData matData in Root.Instance.MaterialBank.MaterialsData)
        {
            matData.renderer.AddMaterial(material);
        }
    }

    private void RemoveMaterialOnRenderers(Material material)
    {
        foreach (MaterialData matData in Root.Instance.MaterialBank.MaterialsData)
        {
            matData.renderer.RemoveMaterial(material);
        }
    }

    private void SmoothStep(Action<float> floatDelegate, float valueStart, float valueEnd, float time, Action executeAfter = null) {
        StartCoroutine(SmoothStepCoroutine(floatDelegate, valueStart, valueEnd, time, executeAfter));
    }

    private IEnumerator SmoothStepCoroutine(Action<float> floatDelegate, float valueStart, float valueEnd, float time, Action executeAfter) {
        float elapsedTime = 0f;
        while (elapsedTime < time) {
            float value = Mathf.SmoothStep(valueStart, valueEnd, elapsedTime / time);
            floatDelegate.Invoke(value);
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        floatDelegate.Invoke(valueEnd);
        executeAfter?.Invoke();
    }
}
