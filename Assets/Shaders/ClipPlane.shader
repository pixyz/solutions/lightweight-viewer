﻿Shader "PiXYZ/ClipPlane"
{
    Properties
    {
		_Color("Albedo", Color) = (0, 0, 0, 1)
		_MainTex("Albedo Tex", 2D) = "white" {}

		_MetallicTex("Metallic Tex", 2D) = "white" {}
		_Metallic("Metallic", Range(0, 1)) = 0

		[Normal] _BumpMap("Normal Map", 2D) = "bump" {}

		_Smoothness("Smoothness", Range(0, 1)) = 0
		[HDR]_Emission("Emission", color) = (0,0,0)

		[HDR]_CutoffColor("Cutoff Color", Color) = (1,0,0,0)
    }
    SubShader
    {
		//the material is completely non-transparent and is rendered at the same time as the other opaque geometry
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry"}
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _MetallicTex;
		sampler2D _BumpMap;

		half _Smoothness;
		half3 _Emission;
        half _Glossiness;
        half _Metallic;

		float4 _Plane;
		float4 _CutoffColor;

        fixed4 _Color;

        struct Input
        {
            float2 uv_MainTex;
			float3 worldPos;
			float facing : VFACE;
			float2 uv_BumpMap;
			float3 worldNormal;
			INTERNAL_DATA
        };


        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			//calculate signed distance to plane
			float distance = dot(IN.worldPos, _Plane.xyz);
			distance = distance + _Plane.w;
			//discard surface above plane
			clip(-distance);

			float facing = IN.facing * 0.5 + 0.5;

			//normal color stuff
			fixed4 col = tex2D(_MainTex, IN.uv_MainTex);
			col *= _Color;
			o.Albedo = col.rgb;// * facing;
			
			fixed4 metal = tex2D(_MetallicTex, IN.uv_MainTex);
			o.Metallic = metal.r * _Metallic;
			o.Smoothness = metal.a * _Smoothness;
			
			o.Emission = lerp(_CutoffColor, _Emission, facing);

			float3 worldInterpolatedNormalVector = WorldNormalVector(IN, float3(0, 0, 1));

			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
        }
        ENDCG
    }
    FallBack "Diffuse"
}
