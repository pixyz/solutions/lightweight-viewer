﻿Shader "PiXYZ/Blurred UI" {

	Properties
   {
      [PerRendererData]
      _MainTex("Srpite Texture", 2D) = "white" {}

      _Color("Main Color", COLOR) = (1,1,1,1)

      _Alpha("Alpha", Range(0, 1)) = 0.5

      [IntRange]
		_Iterations("Iterations", Range(0, 8)) = 2

      [IntRange]
		_SamplesPerIteration("Samples Per Iteration", Range(3, 12)) = 8

		_Radius("Radius", Float) = 100.0

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip("Use Alpha Clip", Float) = 0
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Opaque"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest[unity_GUIZTestMode]
		//Blend SrcAlpha OneMinusSrcAlpha
		GrabPass {Tags{"LightMode" = "Always"}}

		Pass
		{
			Name "Default"

			CGPROGRAM
			#pragma vertex vert 
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest 

			#include "UnityCG.cginc"

         static const float Pi2 = 6.28318530718;

			sampler2D _MainTex;
			sampler2D _GrabTexture;
			float4 _GrabTexture_TexelSize;
			float4 _Color;
			float _Alpha;
			float _Radius;
         int _SamplesPerIteration;
			int _Iterations;

			struct appdata
			{
				float4 vertex : POSITION; 
				float4 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 uvgrab : COLOR;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata  v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);  // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
				o.uv = v.texcoord.xy;

#if UNITY_UV_STARTS_AT_TOP
				float scale = -1.0;
#else
				float scale = 1.0;
#endif
				o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;
				o.uvgrab.zw = o.vertex.zw;
				return o;
			}

			float4 frag(v2f  i) : COLOR
			{
				half4 sum = half4(0, 0, 0, 0);

				#define GRABPIXEL(weight, kernelx, kernely) tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(float4(i.uvgrab.x + _GrabTexture_TexelSize.x * kernelx, i.uvgrab.y + _GrabTexture_TexelSize.y * kernely, i.uvgrab.z, i.uvgrab.w))) * weight;
				
            sum += GRABPIXEL(1, 0, 0);

            float spin = 0;

				for (int p = 1; p <= _Iterations; p += 1) {
					float innerRadius = _Radius * (1.0 * p / _Iterations);
               for (float a = spin; a < Pi2 + spin; a += Pi2 / _SamplesPerIteration) {
                  sum += GRABPIXEL(1, innerRadius * cos(a), innerRadius * sin(a));
               }
               spin += Pi2 / _SamplesPerIteration / _Iterations;
				}

            sum = sum / (_Iterations * _SamplesPerIteration + 1);

            return sum * (1 - _Alpha + _Alpha * _Color * tex2D(_MainTex, i.uv));
			}

			ENDCG
		}
	}
}